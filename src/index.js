var uniqueRandomArray = require('unique-random-array')
var pratchettNames = require('./pratchett-names.json');

module.exports = {
  all: pratchettNames,
  random: uniqueRandomArray(pratchettNames)
}